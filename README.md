# hubot-slack-jenkins

This is an alpine based hubot image which includes jenkins plugin and slack adapter. 
The plugin is forked from [hubot-jenkins-enhanced](https://github.com/codeandfury/hubot-jenkins-enhanced)
with'a minor addition line jenkins crumb support (also, as an improvement, 
triggering jobs based on users' permission is planned.)

## Environment Variables:
In order to run a container from the image, you need to provide a bunch of variable listed below:

- **HUBOT_NAME**: Your bot name (default name is tonguc)
- **HUBOT_SLACK_TOKEN**: You need to generate a hubot slack app and get a slack token 
from [http://slackapi.github.io/hubot-slack/](http://slackapi.github.io/hubot-slack/)
- **HUBOT_JENKINS_URL**: Your jenkins url
- **HUBOT_JENKINS_AUTH**: Jenkins credentials in `username:password` format.
- **HUBOT_JENKINS_CRUMB**: If you use CSFR prevention in Jenkins (which is default) you 
need to generate it. To do this you can use a wget command like this:
 
```
    wget -q \
         --auth-no-challenge \
         --user username \
         --password password \
         --output-document - \
    'https://YOUR_JENKINS_URL/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)'
```
 
- **EXTERNAL_SCRIPTS**: Additional hubot plugins you want to run. 
If not provided, hubot-help plugin will be ran as default. 

## How to run

First, grab the [jenkins-enhanced.coffee](https://bitbucket.org/secopstech/hubot-slack-jenkins/src/master/scripts/jenkins-enhanced.coffee)
file and place it on a local folder (which will be binded to the container)

And run the container:

```
    docker run -d \
        --name "tonguc" \
        -e HUBOT_NAME="tonguc" \
            -e HUBOT_SLACK_TOKEN="xoxb-YOUR-SLACK-TOKEN-HERE" \
        -e HUBOT_JENKINS_URL="http://x.x.x.x/" \
        -e HUBOT_JENKINS_AUTH="username:password" \
        -e HUBOT_JENKINS_CRUMB="JENKINS-CRUMB-VALUE-HERE" \
        -e EXTERNAL_SCRIPTS="hubot-help" \
        -p 8989:8989
        -v /PATH/TO/JENKINS/COFFEE/SCRIPT:/home/hubot/scripts \
        secopstech/hubot-slack-jenkins:1.0.2
```